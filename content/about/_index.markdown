---
author: vguhesan
comments: true
date: 2009-05-21 14:54:32+00:00
layout: page
link: http://mythinkpond.com/about/
slug: about
title: About
wordpress_id: 2
---

> This is without a doubt the greatest time in all of human history to be alive! We’ve never had more information, knowledge, opportunity, possibilities, resources, or wealth than we do now. I hope that we can each make a mark in history as the greatest generation that have ever lived! [B+]

&nbsp;

> This blog is a "labor of love" for all things beautiful and interesting WRT technology, curiosity & learning. It is a never ending work in progress... Always improved, always refined, ever growing... It is my journal! 

<br>

{{<img src="/img/common/VenkattGuhesan_Aug2017.jpg" alt="Venkatt Guhesan" width="150"  align="left" class="authorbox__avatar">}}

**Name:** Venkatt Guhesan

**Residence:** Maryland, USA

**Interests:** Live and Love Technology. My tag cloud or categories in this blog speaks for itself.

**Ideals To Live By:** Live Life, Love What You Do, Never Stop Learning, Share Knowledge & Stay Thirsty!

**Electrical & Computer Engineering** by education | **Enterprise Architect & Software Developer** by trade

**Passions:** Linux (CentOS, Red Hat, Gentoo, Open Stack, Docker & Ubuntu) and embedded processors (ARM processors and Microcontrollers)

**Favorite Embedded Projects:** [Arduino](http://www.arduino.cc/), [UDOO](http://www.udoo.org/), [Cubieboard](http://cubieboard.org/), [CuBox](http://www.solid-run.com/cubox), [BeagleBoard ](http://beagleboard.org/), TI's [Zigbee](https://www.sparkfun.com/categories/111) and TI's [Piccolo Controlstick](http://focus.ti.com/mcu/docs/mcuprodtoolsw.tsp?sectionId=95&tabId=1222&familyId=916&toolTypeId=1)

**Work History:**

- Enterprise Architect & Engineer at [Leidos](https://www.leidos.com/) (working as a consultant at the "Modern Software Services" Group at [Social Security Administration - SSA](https://www.ssa.gov/) working to develop the next generation of tools and services to support the modern DevOps pipeline at SSA)
- Software Engineer & Architect at **ASC Tickets** - developed some unique tools and frameworks for transforming the ticket broker industry.
- Enterprise UI Architect for [DataDirect Networks](http://www.ddn.com). Developed [DDNInsight](https://www.ddn.com/products/storage-monitoring-ddn-insight/), an Enterprise Monitoring and Management Solution for all of the DDN products. 
- Enterprise Managing Architect [DrFirst](http://www.drfirst.com/e-prescribing-for-controlled-substances.jsp) engineering an e-prescribing platform for controlled substances. 
- Thirteen years at [Aon-Hewitt](http://www.aon.com/human-capital-consulting/benefits-admin/default.jsp) developed a premier suite of solutions for Human Resource and Benefits Management under the name RealLifeBenefits.<br/><br/>

I have been lucky and blessed to be diversified in a wide variety of sectors including DevOps, Finance, Investing, Mutual Fund Management, Insurance, Health Care, Rx Management, Ticket Broker Network, Publishing, High Performance Computing, Storage, File Systems (GPFS & Lustre) Management and last but more recently - Software Development Analytics and Reporting.
<br>

**Family:** wife, two daughters and a puppy (very blessed indeed)

{{<img src="/img/2009/05/guhesan_family_photo_sml.jpg" alt="Family Pic 2" width="250" align="center">}}

{{<img src="/img/2019/09/05/IMG_5496.jpg" alt="daughter-2 & puppy" width="250" align="center" class="imgframe">}}

{{<img src="/img/2019/09/05/IMG_4224.jpg" alt="daughter-1" width="400" align="center" class="imgframe">}}

{{<img src="/img/common/OurDog.jpg" alt="Our Puppy Dog" width="450" align="center" class="imgframe">}}

**Blog Mascot:** 
{{<img src="/img/common/owl-mascot.png" alt="Blog Mascot - Owl" width="50" align="center" class="imgframe">}} **Mr.Owl** Owls are considered to be one of the most mysterious of all birds. They are believed to be part of the spirit world that brings both men and the gods together as one. Greek mythology often talks about the goddess Athena who was an owl. Athena is talked about in a good light with the ideals of art, wisdom, beauty, and skillfulness. In some other cultures, owls represent as keepers of wisdom and knowledge and so I felt that Mr.Owl suits best to be the mascot of this website (as the "keeper of the knowledge contained within My-Think-Pond") So as you bookmark this website and when you revisit - you will be reminded of Mr.Owl :-) (Mr.Owl is always hiding in plain sight between the lines in this blog - if you find him - wish him well!) 

I hope you find my collections of thoughts, ideas and journal useful. Don't forget to subscribe to my email on the right side :-)

> Always Remember ... **Confidence** gives you Life, **Fear** brings you closer to Death! (- _my grandfather_)<br>
> and know that all things are possible with perseverance and willpower!

<br/>

Cheers...

-- Last updated on September 8th, 2019

## About this blog (MyThinkPond.com) 

MyThinkPond is developed as a static website using [Hugo](https://gohugo.io/) (static site generator). It is easy to use and is built on one of my favorite languages - [GoLang](https://golang.org/). The theme used on this website is based on [Mainroad](https://github.com/Vimux/Mainroad/). [Markdown](https://en.wikipedia.org/wiki/Markdown) is the markup language used to edit and generate this website content.

  * Syntax Highlighting provided by [prism.js](https://prismjs.com)
  * Search provided by FreeFind Search
  * Analytics provided by Google Analytics
  * Comments System is built using [Disqus](https://disqus.com/)
  * Google Firebase is used for hosting the static website
  * Email Subscription provided by [SendGrid.com](https://sendgrid.com)
  * GIT source for MyThinkPond is hosted on BitBucket.




