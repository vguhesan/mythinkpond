---
author: vguhesan
categories:
- HTML5
comments: true
date: 2012-02-25T18:36:56Z
link: http://mythinkpond.com/2012/02/25/minimal-maintainable-html5-page-template-index-html/
slug: minimal-maintainable-html5-page-template-index-html
tags:
- Hello World
- index.html template
- sample index.html
title: Minimal Maintainable HTML5 Page - Template - index.html
url: /2012/02/25/minimal-maintainable-html5-page-template-index-html/
wordpress_id: 323
---

Sometimes I keep searching for a starter HTML5 "hello-world" - index.html page. So I thought I'd blog about it so that others who need it can also take advantage of it.

[sourcecode language="jscript"]
<!DOCTYPE html>
<html lang=en>
<head>
<meta charset=utf-8>
<title>Sample HTML5 index.html from MyThinkPond</title>
</head>
<body>
<p>Sample HTML5 index.html from MyThinkPond.wordpress.com</p>
</body>
</html>
[/sourcecode]

Feel free to copy and use as needed.

At some point of time, you're going to need something more than a 'hello-world' and when you cross that road go download the "[HTML5 Boilerplate](http://html5boilerplate.com/)". Below is a quote from their site on what it's all about.



<blockquote>HTML5 Boilerplate is the professional badass's base HTML/CSS/JS template for a fast, robust and future-safe site.</blockquote>


Cheers!
