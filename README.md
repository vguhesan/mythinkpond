# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Notes ###

git clone https://github.com/vimux/mainroad ./themes/mainroad
hugo server -D --bind=0.0.0.0 --baseURL=http://0.0.0.0:1313 --disableFastRender

To generate static public content run:
hugo
Copy /public/** to static hosting website

To write a new blog post:
hugo new post/2017-10-28-{title}.md
EXAMPLE:
$ hugo new post/2019-09-06-Spring-Framework-Three-Ways-To-Application-Context.md

Google Firebase Hosting:
https://firebase.google.com/docs/hosting/quickstart

$ firebase init

# Generated updated version of public directory
$ hugo --theme=mainroad
# Deploys content under public
$ firebase deploy

# Syntax Highlighting
https://gohugo.io/content-management/syntax-highlighting/
{{<highlight java "linenos=table" >}}
YOUR_CODE_GOES_HERE
{{</highlight >}}

# Manage Social Postings using Hootsuite
https://hootsuite.com/dashboard#/publisher

